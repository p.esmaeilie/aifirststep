from copy import deepcopy
import json


class Simulator:
    def __init__(self, map, agent_loc):
        self.map = map
        self.agent_loc = agent_loc
    
    def take_action(self, action):
        i,j =self.agent_loc
        if(action == 'L'):
            self.agent_loc =[i,j-1]
        if(action == 'R'):
            self.agent_loc =[i,j+1]
        if(action == 'U'):
            self.agent_loc =[i-1,j]
        if(action == 'D'):
            self.agent_loc =[i+1,j]
        if(action== 'C'):
            self.map[i][j] = 0



class Interface:
    def __init__(self):
        pass

    # an example for what this class is supposed to do
    # here, it will make sure the action that is being
    # requested is in a correct format. this func won't return anything
    # the actual simulator must only deal with the game logic and nothing more
    def evolve(self, state, action):
        if type(action) is not str: raise("action is not a string")
        action=action.upper()
        if action not in self.valid_actions(state): raise("action is not valid")
        state.take_action(action)

    # another example for what this class is supposed to do
    # note that the variables like map and agent_loc are references
    # meaning changing the copied state will also change the original state
    # to copy the actual data, you must use something like deepcopy
    # note that deepcopy is not perfect and is both slow and might not fully copy everything
    # it recommended to use a custom copy function that copies all and only the data that is needed
    def copy_state(self, state):
        _copy = Simulator(None,None)
        _copy.map = deepcopy(state.map)
        _copy.agent_loc = deepcopy(state.agent_loc)
        return _copy

    def perceive(self, state):
        pres = '{"map":'+str(state.map)+', "location":'+str(state.agent_loc)+'}'
        return str(pres)

    def goal_test(self, state):
        for row in state.map:
            if(1 in row):
                return False
        return True
    def left(self , state):
        return state.map[state.agent_loc[0]][state.agent_loc[1]-1]
    
    def valid_actions(self, state):
        i,j =state.agent_loc
        actions = []
        if(i >0 and state.map[i-1][j] != -1 ):
            actions.append('U')
        if(i < len(state.map) -1 and state.map[i+1][j] != -1):
            actions.append('D')
        if(j > 0 and state.map[i][j-1] != -1):
            actions.append('L')
        if(j < len(state.map[0])-1 and state.map[i][j+1] != -1):
            actions.append('R')
        if(state.map[i][j] == 1):
            return ['C']
        return actions
    def valid_states(self, state):
        # TODO return list of legal states
        pass